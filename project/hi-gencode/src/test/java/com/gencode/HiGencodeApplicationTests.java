package com.gencode;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ClassUtils;

public class HiGencodeApplicationTests {

	public static void main(String[] a){
		String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
		System.out.print(path);
	}
}
