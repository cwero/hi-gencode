package com.gencode.service;


import com.gencode.mapper.TableMapper;
import com.gencode.util.MysqlStructure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class TableService {

    @Autowired
    private TableMapper tableMapper;


    public List<MysqlStructure> getTableInfo(String tableName){
        List<MysqlStructure> tableInfo = tableMapper.getTableInfo(tableName);
        return tableInfo;
    }

    public List<String> getTables(){
        return tableMapper.getTables();
    }

}
