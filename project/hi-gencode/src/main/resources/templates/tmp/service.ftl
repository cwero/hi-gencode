package ${packageName}.service;


import ${packageName}.entity.${className}Entity;
import ${packageName}.mapper.${className}Mapper;
import ${packageName}.util.ParamsData;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ${className}Service {

    @Autowired
    private ${className}Mapper ${className?uncap_first}Mapper;


    public List<${className}Entity> getList(ParamsData<${className}Entity.Query,${className}Entity.Order> paramOrderPage){
        List<${className}Entity> list = ${className?uncap_first}Mapper.getList(paramOrderPage);
        return list;
    }

    public PageInfo<${className}Entity> getPageList(ParamsData<${className}Entity.Query,${className}Entity.Order> paramOrderPage){
        PageInfo pageInfo = paramOrderPage.getPageInfo();
        PageHelper.startPage(pageInfo.getPageNum(),pageInfo.getPageSize());
        List<${className}Entity> list = ${className?uncap_first}Mapper.getList(paramOrderPage);
        PageInfo<${className}Entity> result=new PageInfo<${className}Entity>(list);
        return result;
    }


    public ${className}Entity getById(${primaryKeyType} id){
        return ${className?uncap_first}Mapper.getById(id);
    }

    @Transactional
    public boolean insert(${className}Entity ${className?uncap_first}){
        int num = ${className?uncap_first}Mapper.insert(${className?uncap_first});
        System.out.println("生成id:"+${className?uncap_first}.getId());
        if(num>0){
            return true;
        }
            return false;
    }

    @Transactional
    public boolean updateById(${className}Entity ${className?uncap_first}){
        int num = ${className?uncap_first}Mapper.updateById(${className?uncap_first});
        if(num>0){
            return true;
        }
            return false;
    }


    @Transactional
    public boolean deleteById(${primaryKeyType} id){
        int num = ${className?uncap_first}Mapper.deleteById(id);
        if(num>0){
            return true;
        }
            return false;
    }

}
